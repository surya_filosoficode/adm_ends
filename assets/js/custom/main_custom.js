

// copy clipboart
function copyTextToClipboard(text) {
    // navigator.clipboard.writeText(text).then(function() {
    //     console.log('Async: Copying to clipboard was successful!');
    // }, function(err) {
    //     console.error('Async: Could not copy text: ', err);
    // });
    var inp_clip = "<input type=\"text\" name=\"myInput\" id=\"myInput\" value=\""+text+"\">";
    $("#for_clip").html(inp_clip);

    var copyText = document.getElementById("myInput");
    // console.log(copyText);
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
}

    
// toast alert
function create_alert(title, msg, status){
    $(function() {
        "use strict";                      
       $.toast({
        heading: title,
        text: msg,
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: status,
        hideAfter: 3500, 
        stack: 6
      });
    });
}

// sweet alert
function create_sweet_alert(title, msg, status, next_url) {
    ! function($) {
        "use strict";
        // var next = swal.close();
        var SweetAlert = function() {};
        if(status == "success"){
            SweetAlert.prototype.init = function() {
                swal({   
                    title: title,   
                    text: msg,   
                    type: status,   
                    showCancelButton: false,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Lanjutkan",   
                    cancelButtonText: "Perbaiki",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {

                        window.location.href = next_url;  
                    } 
                });
            },
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
        }else {
            SweetAlert.prototype.init = function() {
                swal({   
                    title: title,   
                    text: msg,   
                    type: status,   
                    showCancelButton: false,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Perbaiki",   
                    cancelButtonText: "Perbaiki",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {
                        swal.close();
                    } 
                });
            },
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
        }
        
        
        //init
        
    }(window.jQuery),

    function($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);
}


// sweet alert for delete
function create_sweet_alert_for_del(title, msg, status, next_url) {
    if(status == "success"){
        swal({   
            title: title,   
            text: msg,   
            type: status,   
            showCancelButton: false,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Lanjutkan",   
            cancelButtonText: "Perbaiki",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {

                window.location.href = next_url;  
            } 
        });
    }else {
        swal({   
            title: title,   
            text: msg,   
            type: status,   
            showCancelButton: false,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Perbaiki",   
            cancelButtonText: "Perbaiki",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {
                swal.close();
            } 
        });
    }
}