-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2020 at 03:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fc_ends`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` char(32) NOT NULL,
  `id_tipe_user` varchar(3) NOT NULL,
  `email_user` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `tlp_user` varchar(13) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active_user` enum('0','1','2') NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `alamat_user` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `id_tipe_user`, `email_user`, `username`, `tlp_user`, `password`, `status_active_user`, `nama_user`, `alamat_user`, `is_delete`) VALUES
('UFL2020052900002', '0', 'surya@gmail.com', 'surya', '081230695774', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya', 'malang', '0'),
('UFL2020052900003', '0', 'suryaxx@gmail.com', 'suryas', '081230695773', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya', 'malang', '0');

--
-- Triggers `user`
--
DELIMITER $$
CREATE TRIGGER `gan_insert_usr` BEFORE INSERT ON `user` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index
    INTO rowcount
    FROM index_number where id = 'user';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'user';
    
    SET fix_key_user = concat("UFL",left(NOW()+0, 8),"",LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_user = fix_key_user;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
