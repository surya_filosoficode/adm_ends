-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2020 at 03:27 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fc_ends`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(32) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('AFL2020052300001', '0', 'suryahanggara@gmail.com', 'surya', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya hanggara', 'a', '0'),
('AFL2020052300002', '0', 'vidya@gmail.com', 'vidya', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'vidya meidita', 'a', '0');

--
-- Triggers `admin`
--
DELIMITER $$
CREATE TRIGGER `gan_insert_adm` BEFORE INSERT ON `admin` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index
    INTO rowcount
    FROM index_number where id = 'admin';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'admin';
    
    SET fix_key_user = concat("AFL",left(NOW()+0, 8),"",LPAD(rowcount_next, 5, '0'));
    
    SET NEW.id_admin = fix_key_user;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
