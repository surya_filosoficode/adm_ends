<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


#-----------------------------autentication---------------------------
	$route['login'] 		= 'login/loginv0';
	$route['logout'] 		= 'login/logout';
#-----------------------------autentication---------------------------


#-----------------------------master----------------------------------
	$route['admin/data_admin'] 		= 'admin/adminmain';
	
	$route['admin/data_user'] 		= 'admin/usermain/index';

	$route['admin/data_suplier'] 	= 'admin/supliermain';
	$route['admin/data_store'] 		= 'admin/storemain';
	$route['admin/data_brand'] 		= 'admin/brandmain';
	$route['admin/data_rekanan'] 	= 'admin/rekananmain';
	$route['admin/data_sales'] 		= 'admin/salesmain';

	$route['admin/data_jenis_item'] = 'admin/itemmain/index_jenis';
	$route['admin/data_item'] 		= 'admin/itemmain/index';

	$route['admin/data_image'] 		= 'admin/Imagemain/index';
	$route['admin/data_image_list'] = 'admin/Imagemain/index_list';

	$route['admin/category'] = 'admin/imagemain/index_category';

	$route['admin/toko_main'] 		= 'admin/tokomain/index';
	$route['admin/toko_list'] 		= 'admin/tokomain/index_list';

	$route['admin/brand'] 			= 'admin/productbrand/index';
	$route['admin/jenis_product'] 	= 'admin/productjenis/index';


	$route['admin/jenis_article'] 	= 'admin/articlejenis/index';
	$route['admin/tipe_article'] 	= 'admin/articletipe/index';

	$route['admin/data_post_article'] 	= 'admin/articlemain/index';
	$route['admin/article_list'] 		= 'admin/articlemain/index_list';


	$route['admin/choose_toko'] 		= 'admin/productmain/index_choose_toko';
	$route['admin/product_list'] 		= 'admin/productmain/index_list';
	$route['admin/product_toko'] 		= 'admin/productmain/index_toko';
	$route['admin/product_list_af_toko/(:any)'] = 'admin/productmain/index_list_af_toko/$1';
	
	$route['admin/main_product/(:any)'] = 'admin/productmain/index/$1';

	$route['admin/article_list'] 		= 'admin/articlemain/index_list';
#-----------------------------master----------------------------------


#-----------------------------promote----------------------------------
	$route['admin/promote_article'] = 'admin/promotemain/index_pr_srt_list';

	$route['admin/home_page_content'] = 'admin/promotepagehome/index_list';
#-----------------------------promote----------------------------------



#-----------------------------user----------------------------------
	
	$route['user/login'] 		= 'login/loginuser/index';
	$route['user/logout'] 		= 'login/logout/logout_user';


	$route['user/count/home'] 	= 'user/userfront/index';
#-----------------------------user----------------------------------

