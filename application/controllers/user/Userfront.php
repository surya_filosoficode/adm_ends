<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userfront extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/main_user', 'mu');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
    }

#===============================================================================
#-----------------------------------home_user----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "user_home";
		// $data["list_data"] = $this->mm->get_data_all_where("user", array("is_delete"=>"0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_user----------------------------------
#===============================================================================

}
