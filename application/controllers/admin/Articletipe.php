<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articletipe extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        $this->load->library("magic_pattern");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function index(){
        $data["page"] = "article_tipe";
        $data["list_data"] = $this->mm->get_data_all_where("article_tipe", []);

        $this->load->view("index", $data);
    }

    public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'nama_art_tipe',
                    'label'=>'nama_art_tipe',
                    'rules'=>'required|is_unique[article_tipe.nama_art_tipe]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "nama_art_tipe"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_insert()){
            $nama_art_tipe     = strtolower($this->input->post("nama_art_tipe"));
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_art_tipe]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $data = ["id_art_tipe"     => "",
                         "nama_art_tipe"   => $nama_art_tipe,
                         "is_del_art_tipe" => "0"];

                $insert = $this->mm->insert_data("article_tipe", $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["nama_art_tipe"]= strip_tags(form_error('nama_art_tipe'));
        }


        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_art_tipe"])){
            $id_art_tipe = $this->input->post('id_art_tipe');
            $data = $this->mm->get_data_each("article_tipe", array("id_art_tipe"=>$id_art_tipe));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_update(){
        $config_val_input = array(
                array(
                    'field'=>'id_art_tipe',
                    'label'=>'id_art_tipe',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'nama_art_tipe',
                    'label'=>'nama_art_tipe',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "id_art_tipe"=>"",
                    "nama_art_tipe"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_update()){
            $id_art_tipe       = $this->input->post("id_art_tipe");

            $nama_art_tipe     = strtolower($this->input->post("nama_art_tipe"));
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_art_tipe],
                            [$type_pattern, $nama_art_tipe]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $where = ["id_art_tipe"=> $id_art_tipe];

                $data = ["nama_art_tipe"=> $nama_art_tipe];

                $check_category = $this->mm->get_data_each("article_tipe", ["nama_art_tipe"=>$nama_art_tipe, "id_art_tipe!="=>$id_art_tipe]);
                if($check_category){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }else{
                    $insert = $this->mm->update_data("article_tipe", $data, $where);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["id_art_tipe"]     = strip_tags(form_error('id_art_tipe'));
            $msg_detail["nama_art_tipe"]   = strip_tags(form_error('nama_art_tipe'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array(
                    "id_art_tipe"=>"");
        if(isset($_POST['id_art_tipe'])){
            $id_art_tipe = $this->input->post("id_art_tipe", true);
            
                
            if($this->mm->delete_data("article_tipe", ["id_art_tipe"=>$id_art_tipe])){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

}
