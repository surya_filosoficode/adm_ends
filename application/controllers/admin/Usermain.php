<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermain extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('main/store_insert_auto_key', 'auto_key');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_user----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "user_main";
		$data["list_data"] = $this->mm->get_data_all_where("user", array("is_delete"=>"0"));
		$this->load->view('index', $data);
        // print_r($data);
	}
#===============================================================================
#-----------------------------------home_user----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_user--------------------------------
#===============================================================================
	public function val_form_insert_user(){
        $config_val_input = array(
                array(
                    'field'=>'email_user',
                    'label'=>'email_user',
                    'rules'=>'required|is_unique[user.email_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|is_unique[user.username]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'tlp_user',
                    'label'=>'tlp_user',
                    'rules'=>'required|is_unique[user.tlp_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_user',
                    'label'=>'nama_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'alamat_user',
                    'label'=>'alamat_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    ) 
                ),array(
                    'field'=>'repassword',
                    'label'=>'repassword',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "email_user"=>"",
                    "username"=>"",
                    "tlp_user"=>"",

                    "nama_user"=>"",
                    "alamat_user"=>"",                   

                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_insert_user()){
            $id_tipe_user   = $this->input->post("id_tipe_user", true);

            $email_user     = $this->input->post("email_user", true);
            $username       = $this->input->post("username", true);
            $tlp_user       = $this->input->post("tlp_user", true);

            $nama_user 	    = $this->input->post("nama_user", true);
            $alamat_user    = $this->input->post("alamat_user", true);
            
            $password       = $this->input->post("password", true);
            $repassword     = $this->input->post("repassword", true);


            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_tipe_user],
                                [$type_pattern, $email_user],
                                [$type_pattern, $username],
                                [$type_pattern, $tlp_user],
                                [$type_pattern, $nama_user],
                                [$type_pattern, $password]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                if($password == $repassword){
                    // $insert = $this->auto_key->user_insert($id_tipe_user, $email_user, $username, $tlp_user, $nama_user, $alamat_user, hash("sha256", $password));
                    $data = ["id_user"=>"",
                            "id_tipe_user"=>$id_tipe_user,
                            "email_user"=>$email_user,
                            "username"=>$username,
                            "tlp_user"=>$tlp_user,
                            "password"=>hash("sha256", $password),
                            "status_active_user"=>"0",
                            "nama_user"=>$nama_user,
                            "alamat_user"=>$alamat_user,
                            "is_delete"=>"0"
                        ];
                    $insert = $this->mm->insert_data("user", $data);

                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }else{
                    $msg_main = array("status"=>false, "msg"=>"ulangi password dengan benar");
                }
            }
        }else{
            $msg_detail["email_user"]   = strip_tags(form_error('email_user'));
            $msg_detail["username"]     = strip_tags(form_error('username'));
            $msg_detail["tlp_user"]     = strip_tags(form_error('tlp_user'));
            
            $msg_detail["nama_user"]    = strip_tags(form_error('nama_user'));
            $msg_detail["alamat_user"] 	= strip_tags(form_error('alamat_user'));           
            
            $msg_detail["password"]     = strip_tags(form_error('password'));
            $msg_detail["repassword"]   = strip_tags(form_error('repassword'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_user--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id"])){
        	$id = $this->input->post('id');
        	$data = $this->mm->get_data_each("user", array("id_user"=>$id, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_user--------------------------------
#===============================================================================
    public function val_form_update_user(){
        $config_val_input = array(
                array(
                    'field'=>'id',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'email_user',
                    'label'=>'email_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'tlp_user',
                    'label'=>'tlp_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_user',
                    'label'=>'nama_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'alamat_user',
                    'label'=>'alamat_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
  
    public function update_user(){

        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_tipe_user"=>"",

                    "email_user"=>"",
                    "username"=>"",
                    "tlp_user"=>"",
                    "nama_user"=>"",
                    "alamat_user"=>""
                );

        if($this->val_form_update_user()){
        	$id 		= $this->input->post("id");

            $id_tipe_user   = $this->input->post("id_tipe_user", true);
            
            $email_user     = $this->input->post("email_user", true);
            $username       = $this->input->post("username", true);
            $tlp_user       = $this->input->post("tlp_user", true);

            $nama_user      = $this->input->post("nama_user", true);
            $alamat_user    = $this->input->post("alamat_user", true);

          	// check username
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_tipe_user],
                                [$type_pattern, $email_user],
                                [$type_pattern, $username],
                                [$type_pattern, $tlp_user],
                                [$type_pattern, $nama_user]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                if(!$this->mm->get_data_each("user", array("username"=>$username, "id_user!="=>$id))){
                    // print_r("username done");
                    if(!$this->mm->get_data_each("user", array("tlp_user"=>$tlp_user, "id_user!="=>$id))){
                        // print_r("tlp_user done");
                        if(!$this->mm->get_data_each("user", array("email_user"=>$email_user, "id_user!="=>$id))){
                             // print_r("email_user done");
                            $set = array(
                                "id_tipe_user"  =>$id_tipe_user,
                                "email_user"    =>$email_user,
                                "tlp_user"      =>$tlp_user,
                                "username" =>$username,
                                "nama_user"     =>$nama_user,
                                "alamat_user"   =>$alamat_user
                            );

                            $where = array("id_user"=>$id);

                            $update = $this->mm->update_data("user", $set, $where);
                            if($update){
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                            }
                        }else{
                            $msg_detail["email_user"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
                        }
                    }else{
                        $msg_detail["tlp_user"] = $this->response_message->get_error_msg("TLP_AVAIL");
                    }
                }else{
                    $msg_detail["username"] = $this->response_message->get_error_msg("USERNAME_AVAIL");
                }   
            }
        }else{
            $msg_detail["id_user"]      = strip_tags(form_error('id_user'));
            $msg_detail["id_tipe_user"] = strip_tags(form_error('id_tipe_user'));

            $msg_detail["email_user"]   = strip_tags(form_error('email_user'));
            $msg_detail["username"]     = strip_tags(form_error('username'));
            $msg_detail["tlp_user"]     = strip_tags(form_error('tlp_user'));
            
            $msg_detail["nama_user"]    = strip_tags(form_error('nama_user'));
            $msg_detail["alamat_user"]  = strip_tags(form_error('alamat_user'));          
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_user--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_user--------------------------------
#===============================================================================

    public function delete_user(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id"=>"",
                );

        if(isset($_POST["id"])){
            if($_POST["id"]){
                $id = $this->input->post("id");
                $where = array("id"=>$id);

                $set = array("is_delete"=>"1");
                $where = array("id_user"=>$id);

                $delete_user = $this->mm->update_data("user", $set, $where);
                
                if($delete_user){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_user--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------change_password_user-----------------------
#===============================================================================
    public function val_form_ch_pass_user(){
        $config_val_input = array(
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass_user(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_ch_pass_user()){
        	$id 		    = $this->input->post("id");
            $password 		= $this->input->post("password");
            $repassword 	= $this->input->post("repassword");

          	// check username
          	if($password == $repassword){
          		$set = array(
          				"password"=>hash("sha256", $password)
          			);

          		$where = array("id_user"=>$id);

          		$update = $this->mm->update_data("user", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_main = array("status"=>false, "msg"=>"ulangi password dengan benar");
          	}
        }else{
            $msg_detail["password"] 	= strip_tags(form_error('password'));
            $msg_detail["repassword"] 	= strip_tags(form_error('repassword'));         
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password_user-----------------------
#===============================================================================


#===============================================================================
#-----------------------------------dasabled_user------------------------------
#===============================================================================

    public function disabled_user(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                );

        if($_POST["id"]){
        	$id = $this->input->post("id");
        	$where 	= array("id_user"=>$id);
        	$set 	= array("status_active_user"=>"0");
        	
        	$update_user = $this->mm->update_data("user", $set, $where);
        	if($update_user){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user"]= strip_tags(form_error('id_user'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_user------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------activate_user------------------------------
#===============================================================================
    public function activate_user(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                );

        if($_POST["id"]){
        	$id = $this->input->post("id");
        	$where 	= array("id_user"=>$id);
        	$set 	= array("status_active_user"=>"1");
        	
        	$update_user = $this->mm->update_data("user", $set, $where);
        	if($update_user){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user"]= strip_tags(form_error('id_user'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_user------------------------------
#===============================================================================


}
