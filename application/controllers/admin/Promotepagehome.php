<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotepagehome extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('promote/main_promote', 'mp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        $this->load->library("Time_master");
    }
 

    public function index(){
        $data["page"] = "promote_page_home";
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_jenis"] = $this->mm->get_data_all_where("article_jenis", []);
        
        $this->load->view('index', $data);
    }

    public function index_list(){
        $data["page"] = "promote_page_home";

        $data["list_data"] = $this->mm->get_data_all_where("product_jenis", ["is_delete_jenis"=>"0"]);
        $data["list_data_pr"] = $this->mp->get_promote_pg_home_all([]);
        // print_r("<pre>");
        // print_r($data);
        $this->load->view('index', $data);
    }


    public function val_save(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id_jenis',
                    'rules'=>'required|is_unique[pr_page_home.id_jenis]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("USER_IN_TOKO_AVAIL")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function add_promote(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_jenis"=>""
                );

        if($this->val_save()){
            $id_jenis     = $this->input->post("id_jenis", true);

            $create_date_article    = date("Y-m-d H:i:s");
            $create_admin_article   = $_SESSION["ih_mau_ngapain"]["id_admin"];

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_jenis]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $data = ["id_pr"    =>"",
                        "id_jenis"  =>$id_jenis,
                        "align_header_pr"=>"left",
                        "r_num_pr"  =>"0",
                        "tgl_add_pr"=>$create_date_article,
                        "add_by_pr" =>$create_admin_article,
                        "sts_pr"    =>"0",
                        "active_pr" =>"1"
                    ];

                $insert = $this->mm->insert_data("pr_page_home", $data);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["id_jenis"]    = strip_tags(form_error('id_jenis'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_pr"])){
            $id_pr = $this->input->post('id_pr');
            $data = $this->mp->get_promote_pg_home_each(array("id_pr"=>$id_pr));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_update(){
        $config_val_input = array(
                array(
                    'field'=>'id_pr',
                    'label'=>'id_pr',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'letak_header',
                    'label'=>'letak_header',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'urutan_category',
                    'label'=>'urutan_category',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'status_promote',
                    'label'=>'status_promote',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'status_active',
                    'label'=>'status_active',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_pr"=>"",
                    "letak_header"=>"",
                    "urutan_category"=>"",
                    "status_promote"=>"",
                    "status_active"=>""
                );

        if($this->val_update()){
            $id_pr          = $this->input->post("id_pr", true);

            $letak_header   = $this->input->post("letak_header", true);
            $urutan_category= $this->input->post("urutan_category", true);
            $status_promote = $this->input->post("status_promote", true);
            $status_active  = $this->input->post("status_active", true);

            $create_date_article    = date("Y-m-d H:i:s");
            $create_admin_article   = $_SESSION["ih_mau_ngapain"]["id_admin"];

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_pr],
                             [$type_pattern, $letak_header],
                             [$type_pattern, $urutan_category],
                             [$type_pattern, $status_promote],
                             [$type_pattern, $status_active]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $data = ["align_header_pr"  =>$letak_header,
                         "r_num_pr"         =>$urutan_category,
                         "sts_pr"           =>$status_promote,
                         "active_pr"        =>$status_active
                        ];

                $where = ["id_pr"=>$id_pr];

                $update = $this->mm->update_data("pr_page_home", $data, $where);

                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else{
            $msg_detail["id_pr"]            = strip_tags(form_error('id_pr'));
            $msg_detail["letak_header"]     = strip_tags(form_error('letak_header'));
            $msg_detail["urutan_category"]  = strip_tags(form_error('urutan_category'));
            $msg_detail["status_promote"]   = strip_tags(form_error('status_promote'));
            $msg_detail["status_active"]    = strip_tags(form_error('status_active'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_pr"=>"",
                );

        if($_POST["id_pr"]){
            $id_pr = $this->input->post("id_pr", true);

            $where = array("id_pr"=>$id_pr);

            // $delete_pr = $this->mm->delete_data("toko", array("id_pr"=>$id_pr));
            $delete_pr = $this->mm->delete_data("pr_page_home", $where);
            
            if($delete_pr){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_pr"]= strip_tags(form_error('id_pr'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


}
