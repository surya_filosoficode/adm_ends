<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagemain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('main/store_insert_auto_key', 'auto_key');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");

        $this->load->library("magic_pattern");

        date_default_timezone_set("Asia/Bangkok");
    }

    public function index(){
        $data["page"] = "image_main";
        $data["list_data"] = $this->mm->get_data_all_where("m_img_category", []);

        $this->load->view("index", $data);
    }

    public function index_list(){
        $data["page"] = "image_list";

        $data["list_cate"] = $this->mm->get_data_all_where("m_img_category", []);
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        if(isset($_GET["cate"])){
            if($_GET["cate"]){
                $cate = $_GET["cate"];
                $data["list_image"] = $this->db->query("select * from m_img WHERE category_img like '%$cate%'")->result();
            }
        }

        $this->load->view("index", $data);
    }


    public function val_form_insert_img(){
        $config_val_input = array(
                array(
                    'field'=>'data_img',
                    'label'=>'data_img',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save_image(){
        // error_reporting(E_ALL);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "data_img"=>"");

        $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_insert_img()){
            $data_img = json_decode($this->input->post("data_img"));
            // print_r($data_img);
            $date_insert = date("Y-m-d H:i:s");

            $admin = "-";

            $array_status = [];

            // print_r($data_img);
            foreach ($data_img as $key => $value) {
                $main_img       = $value->main_img;
                $title_img      = $value->title_img;
                $category_img   = $value->category_img;
                $jenis_img      = $value->img_for;

                // print_r("<pre>");
                // print_r($value);

                if($main_img){
                    $data = $main_img;
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);

                    $date_now = date("Ymdhis");

                    $path_img ='assets/img/all_img/';
                    $file_img ='img_'.$date_now.'_'.$key.'.png';
                    $date_img = date("Y-m-d h:i:s");

                    $tipe_owner_img = "admin";
                    $owner_img = $id_admin;

                    // print_r($owner_img);

                    // print_r($value);

                    $status = false;

                    $create_file = file_put_contents($path_img.$file_img, $data);
                    if($create_file){
                        $insert = $this->auto_key->image_insert($title_img, $path_img, $file_img, json_encode($category_img), $date_img, $owner_img, $tipe_owner_img, $jenis_img);
                        if($insert){
                            $status = true;
                        }
                    }

                    // array_push($array_status, $status);
                }
            }

            if(!in_array(false, $array_status)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_image(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array(
                    "param"=>"");
        if(isset($_POST['param'])){
            $param = $this->input->post("param", true);
            
            $cek_data_img = $this->mm->get_data_each("m_img", ["id_img"=>$param]);
            if($cek_data_img){
                $file = $cek_data_img["file_img"];
                $dir = $cek_data_img["path_img"];

                // print_r($dir.$file);
                // if(file_exists($dir.$file)){
                //     unlink("./".$dir.$file);
                //     // $status = true;
                //     print_r("ada");
                // }else {
                //     print_r("tidak");
                // }
                $this->load->library("uploadfilev0");
                if($this->uploadfilev0->delete_file($dir, $file)){
                    if($this->mm->delete_data("m_img", ["id_img"=>$param])){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                    }
                }
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }



    public function index_category(){
        $data["page"] = "category_main";
        $data["list_data"] = $this->mm->get_data_all_where("m_img_category", []);

        $this->load->view("index", $data);
    }

    public function val_form_insert_img_ct(){
        $config_val_input = array(
                array(
                    'field'=>'ket_category',
                    'label'=>'ket_category',
                    'rules'=>'required|is_unique[m_img_category.ct]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save_image_ct(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "ket_category"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_insert_img_ct()){
            $ket_category = strtolower($this->input->post("ket_category"));
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $ket_category]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $data = ["id_img_ct"=> "",
                         "ct"=> $ket_category,
                         "id_img_ct_base"=> ""];

                $insert = $this->mm->insert_data("m_img_category", $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["ket_category"]= strip_tags(form_error('ket_category'));
        }


        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        $res_msg["list_data"] = $this->mm->get_data_all_where("m_img_category", []);
        print_r(json_encode($res_msg));
    }

    public function get_data_ct(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_img_ct"])){
            $id_img_ct = $this->input->post('id_img_ct');
            $data = $this->mm->get_data_each("m_img_category", array("id_img_ct"=>$id_img_ct));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function val_form_update_img_ct(){
        $config_val_input = array(
                array(
                    'field'=>'id_img_ct',
                    'label'=>'id_img_ct',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'ket_category',
                    'label'=>'ket_category',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_image_ct(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "id_img_ct"=>"",
                    "ket_category"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_update_img_ct()){
            $id_img_ct      = $this->input->post("id_img_ct");
            $ket_category   = strtolower($this->input->post("ket_category"));
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_img_ct],
                            [$type_pattern, $ket_category]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $where = ["id_img_ct"=> $id_img_ct];
                $data = ["ct"=> $ket_category,
                         "id_img_ct_base"=> ""];

                $check_category = $this->mm->get_data_each("m_img_category", ["ct"=>$ket_category, "id_img_ct!="=>$id_img_ct]);
                if($check_category){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
                }else{
                    $insert = $this->mm->update_data("m_img_category", $data, $where);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }
            }
        }else{
            $msg_detail["ket_category"]= strip_tags(form_error('ket_category'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_image_ct(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array(
                    "id_img_ct"=>"");
        if(isset($_POST['id_img_ct'])){
            $id_img_ct = $this->input->post("id_img_ct", true);
            
                
            if($this->mm->delete_data("m_img_category", ["id_img_ct"=>$id_img_ct])){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

}
