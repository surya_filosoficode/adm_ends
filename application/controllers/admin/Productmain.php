<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productmain extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/main_product', 'mp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------index_product-------------------------------
#===============================================================================
	public function index_choose_toko(){
		$data["page"] = "product_choose_toko";
        $data["list_toko"]  = $this->mm->get_data_all_where("toko",["is_delete"=>"0"]);
		
        $this->load->view('index', $data);
	}

    public function index($id_toko = ""){
        $data["page"] = "product_main";

        $data["list_toko"]  = [];
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_brand"] = $this->mm->get_data_all_where("product_brand", ["is_delete_brand"=>"0"]);
        $data["list_jenis"] = $this->mm->get_data_all_where("product_jenis", ["is_delete_jenis"=>"0"]);

        if($id_toko != ""){
            $data["list_toko"]  = $this->mm->get_data_each("toko", ["id_toko"=>$id_toko, "is_delete"=>"0"]);
        }
        
        $this->load->view('index', $data);
    }

    public function index_list(){
        $data["page"] = "product_list";
        $data["list_product"] = $this->mp->get_all_product(["is_delete_product"=>"0"]);

        $this->load->view('index', $data);
    }

    public function index_list_af_toko($id_toko = ""){
        $data["page"] = "product_list_af_toko";

        $data["list_product"] = "";
        if($id_toko != ""){
            $data["list_product"] = $this->mp->get_all_product(["is_delete_product"=>"0", "pd.id_toko"=>$id_toko]);
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

    public function index_update($id_product = ""){
        $data["page"] = "product_update";

        $data["list_toko"]  = [];
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_brand"] = $this->mm->get_data_all_where("product_brand", ["is_delete_brand"=>"0"]);
        $data["list_jenis"] = $this->mm->get_data_all_where("product_jenis", ["is_delete_jenis"=>"0"]);

        $data["list_product"] = $this->mm->get_data_each("product", ["id_product"=> $id_product, "is_delete_product"=>"0"]);

        $id_toko = $data["list_product"]["id_toko"];

        if($id_toko != ""){
            $data["list_toko"]  = $this->mm->get_data_each("toko", ["id_toko"=>$id_toko, "is_delete"=>"0"]);
        }
        
        // print_r("<pre>");
        // print_r($data);
        $this->load->view('index', $data);
    }
#===============================================================================
#-----------------------------------index_product-------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------save_product--------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
                array(
                    'field'=>'nama_product',
                    'label'=>'nama_product',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'toko',
                    'label'=>'toko',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'brand',
                    'label'=>'brand',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),


                array(
                    'field'=>'harga_produk',
                    'label'=>'harga_produk',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'sts_pr_prd',
                    'label'=>'sts_pr_prd',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'disc_produk',
                    'label'=>'disc_produk',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'sts_nego',
                    'label'=>'sts_nego',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),

                array(
                    'field'=>'category_produk',
                    'label'=>'category_produk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'tag_produk',
                    'label'=>'tag_produk',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'article',
                    'label'=>'article',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                ),array(
                    'field'=>'img_list_product',
                    'label'=>'img_list_product',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function save(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "nama_product"=>"",
                    "toko"=>"",
                    "brand"=>"",
                    "harga_produk"=>"",
                    "sts_pr_prd"=>"",
                    "disc_produk"=>"",
                    "sts_nego"=>"",
                    "category_produk"=>"",
                    "tag_produk"=>"",
                    "article"=>"",
                    "img_list_product"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];

        if($this->val_form_insert()){
            $nama_product   = $this->input->post("nama_product", true);
            $toko   = $this->input->post("toko", true);
            $brand   = $this->input->post("brand", true);
            $harga_produk   = $this->input->post("harga_produk", true);
            $sts_pr_prd   = $this->input->post("sts_pr_prd", true);
            $disc_produk   = $this->input->post("disc_produk", true);
            $sts_nego   = $this->input->post("sts_nego", true);
            $category_produk   = $this->input->post("category_produk", true);
            $tag_produk   = $this->input->post("tag_produk", true);
            $article   = $this->input->post("article");
            $img_list_product   = $this->input->post("img_list_product");

            $img_list_product = str_replace(base_url(), "base_url/", $img_list_product);
            $article = str_replace(base_url(), "base_url/", $article);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nama_product],
                             [$type_pattern, $toko],
                             [$type_pattern, $brand],
                             [$type_pattern, $harga_produk]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            }else{
                $data = ["id_product"        => "",
                         "nama_product"      => $nama_product,
                         "id_toko"           => $toko,
                         "id_brand"          => $brand,
                         "category_produk"   => $category_produk,
                         "desc_product"      => $article,
                         "spec_product"      => "",
                         "img_list_product"  => $img_list_product,
                         "tag_product"       => $tag_produk,
                         "price_product"     => $harga_produk,
                         "disc_product"      => $disc_produk,
                         "sts_pr_product"    => $sts_pr_prd,
                         "sts_nego_product"  => $sts_nego,
                         "is_delete_product" => "0"];

                $insert = $this->mm->insert_data("product", $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_detail["nama_product"] = strip_tags(form_error('nama_product'));
            $msg_detail["toko"]         = strip_tags(form_error('toko'));
            $msg_detail["brand"]        = strip_tags(form_error('brand'));
            $msg_detail["harga_produk"] = strip_tags(form_error('harga_produk'));
            $msg_detail["sts_pr_prd"] = strip_tags(form_error('sts_pr_prd'));
            $msg_detail["disc_produk"] = strip_tags(form_error('disc_produk'));
            $msg_detail["sts_nego"] = strip_tags(form_error('sts_nego'));
            $msg_detail["category_produk"]  = strip_tags(form_error('category_produk'));
            $msg_detail["tag_produk"]       = strip_tags(form_error('tag_produk'));
            $msg_detail["article"]          = strip_tags(form_error('article'));
            $msg_detail["img_list_product"] = strip_tags(form_error('img_list_product'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------save_product--------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------up_product----------------------------------
#===============================================================================
    
    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "nama_product"=>"",
                    "toko"=>"",
                    "brand"=>"",
                    "harga_produk"=>"",
                    "sts_pr_prd"=>"",
                    "disc_produk"=>"",
                    "sts_nego"=>"",
                    "category_produk"=>"",
                    "tag_produk"=>"",
                    "article"=>"",
                    "img_list_product"=>"");

        // $id_admin = $this->auth_v0->get_session()["id_admin"];
        if(isset($_POST["id_product"])){
            if($this->val_form_insert()){
                $id_product   = $this->input->post("id_product", true);

                $nama_product   = $this->input->post("nama_product", true);
                $toko   = $this->input->post("toko", true);
                $brand   = $this->input->post("brand", true);
                $harga_produk   = $this->input->post("harga_produk", true);
                $sts_pr_prd   = $this->input->post("sts_pr_prd", true);
                $disc_produk   = $this->input->post("disc_produk", true);
                $sts_nego   = $this->input->post("sts_nego", true);
                $category_produk   = $this->input->post("category_produk", true);
                $tag_produk   = $this->input->post("tag_produk", true);
                $article   = $this->input->post("article");
                $img_list_product   = $this->input->post("img_list_product");

                $img_list_product = str_replace(base_url(), "base_url/", $img_list_product);
                $article = str_replace(base_url(), "base_url/", $article);
                
                $type_pattern   = "allowed_general_char";

                $arr_pattern  = [[$type_pattern, $nama_product],
                                 [$type_pattern, $toko],
                                 [$type_pattern, $brand],
                                 [$type_pattern, $harga_produk]
                                ];

                if($this->magic_pattern->set_list_pattern($arr_pattern)){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                }else{
                    $where = ["id_product"        => $id_product];
                    $data = ["nama_product"      => $nama_product,
                             "id_toko"           => $toko,
                             "id_brand"          => $brand,
                             "category_produk"   => $category_produk,
                             "desc_product"      => $article,
                             "spec_product"      => "",
                             "img_list_product"  => $img_list_product,
                             "tag_product"       => $tag_produk,
                             "price_product"     => $harga_produk,
                             "disc_product"      => $disc_produk,
                             "sts_pr_product"    => $sts_pr_prd,
                             "sts_nego_product"  => $sts_nego,
                             "is_delete_product" => "0"];

                    $insert = $this->mm->update_data("product", $data, $where);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }else{
                $msg_detail["nama_product"] = strip_tags(form_error('nama_product'));
                $msg_detail["toko"]         = strip_tags(form_error('toko'));
                $msg_detail["brand"]        = strip_tags(form_error('brand'));
                $msg_detail["harga_produk"] = strip_tags(form_error('harga_produk'));
                $msg_detail["sts_pr_prd"] = strip_tags(form_error('sts_pr_prd'));
                $msg_detail["disc_produk"] = strip_tags(form_error('disc_produk'));
                $msg_detail["sts_nego"] = strip_tags(form_error('sts_nego'));
                $msg_detail["category_produk"]  = strip_tags(form_error('category_produk'));
                $msg_detail["tag_produk"]       = strip_tags(form_error('tag_produk'));
                $msg_detail["article"]          = strip_tags(form_error('article'));
                $msg_detail["img_list_product"] = strip_tags(form_error('img_list_product'));
            }
        }
            
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------up_product----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------delete_product------------------------------
#===============================================================================

    public function delete(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_product"=>"",
                );

        if($_POST["id_product"]){
            $id_product = $this->input->post("id_product");
            $where = array("id_product"=>$id_product);

            $set = array("is_delete_product"=>"1");
            $where = array("id_product"=>$id_product);

            // $delete = $this->mm->delete_data("product", array("id_product"=>$id_product));
            $delete = $this->mm->update_data("product", $set, $where);
            
            if($delete){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_product"]= strip_tags(form_error('id_product'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_product------------------------------
#===============================================================================


}
