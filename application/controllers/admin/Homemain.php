<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homemain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/Main_admin', 'ma');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $page["home"] = "home_main";
        $this->load->view("index", $data);
    }
}
