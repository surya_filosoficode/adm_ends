
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tokomain extends CI_Controller {

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_toko----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "toko_main";
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_user"]  = $this->mm->get_data_all_where("user", []);
		
        $this->load->view('index', $data);
	}

    public function index_list(){
        // print_r("toko_list");
        $data["page"]       = "toko_list";
        $data["list_toko"]  = $this->mm->get_data_all_where("toko",["is_delete"=>"0"]);
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);

        $this->load->view('index', $data);
    }

    public function index_update($id_toko = ""){
        // print_r("toko_list");
        $data["page"]       = "toko_update";
        $data["list_toko"]  = [];
        $data["list_image"] = $this->mm->get_data_all_where("m_img", []);
        $data["list_user"]  = $this->mm->get_data_all_where("user", []);

        if($id_toko != ""){
            $data["list_toko"]  = $this->mm->get_data_each("toko", ["id_toko"=>$id_toko, "is_delete"=>"0"]);
        }
        
        $this->load->view('index', $data);
    }
#===============================================================================
#-----------------------------------home_toko----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_toko--------------------------------
#===============================================================================
	public function val_form_insert_toko(){
        $config_val_input = array(
                array(
                    'field'=>'tipe_owner',
                    'label'=>'tipe_owner',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'owner',
                    'label'=>'owner',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_toko',
                    'label'=>'nama_toko',
                    'rules'=>'required|is_unique[toko.nama_toko]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("USER_IN_TOKO_AVAIL")
                    ) 
                ),array(
                    'field'=>'desc_toko',
                    'label'=>'desc_toko',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'main_img_toko',
                    'label'=>'main_img_toko',
                    'rules'=>'required|valid_url',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'img_list_toko',
                    'label'=>'img_list_toko',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_toko(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tipe_owner"=>"",
                    "owner"=>"",
                    "nama_toko"=>"",
                    "desc_toko"=>"",
                    "main_img_toko"=>"",
                    "img_list_toko"=>""
                );

        if($this->val_form_insert_toko()){
            $tipe_owner 	= $this->input->post("tipe_owner", true);
            $owner          = $this->input->post("owner", true);
            $nama_toko      = $this->input->post("nama_toko", true);
            $desc_toko      = $this->input->post("desc_toko", true);
            $main_img_toko  = $this->input->post("main_img_toko", true);
            $img_list_toko  = $this->input->post("img_list_toko", true);

            $main_img_toko = str_replace(base_url(), "base_url/", $main_img_toko);
            $img_list_toko = str_replace(base_url(), "base_url/", $img_list_toko);
            $desc_toko = str_replace(base_url(), "base_url/", $desc_toko);

            $data = ["id_toko"      =>"",
                    "tipe_owner"    =>$tipe_owner,
                    "id_owner"      =>$owner,
                    "nama_toko"     =>$nama_toko,
                    "desc_toko"     =>$desc_toko,
                    "main_img_toko" =>$main_img_toko,
                    "img_list_toko" =>$img_list_toko,
                    "is_delete"     =>"0"
                ];

            // print_r($data);
            $insert = $this->mm->insert_data("toko", $data);
            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["tipe_owner"]     = strip_tags(form_error('tipe_owner'));
            $msg_detail["owner"] 		  = strip_tags(form_error('owner'));  
            $msg_detail["nama_toko"]      = strip_tags(form_error('nama_toko'));
            $msg_detail["desc_toko"]      = strip_tags(form_error('desc_toko'));  
            $msg_detail["main_img_toko"]  = strip_tags(form_error('main_img_toko'));
            $msg_detail["img_list_toko"]  = strip_tags(form_error('img_list_toko'));           
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_toko--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_toko"])){
        	$id_toko = $this->input->post('id_toko');
        	$data = $this->mm->get_data_each("toko", array("id_toko"=>$id_toko, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_toko--------------------------------
#===============================================================================
    public function val_form_update_toko(){
        $config_val_input = array(
                array(
                    'field'=>'tipe_owner',
                    'label'=>'tipe_owner',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'owner',
                    'label'=>'owner',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_toko',
                    'label'=>'nama_toko',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'desc_toko',
                    'label'=>'desc_toko',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'main_img_toko',
                    'label'=>'main_img_toko',
                    'rules'=>'required|valid_url',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'img_list_toko',
                    'label'=>'img_list_toko',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }
  
    public function update_toko(){
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tipe_owner"=>"",
                    "owner"=>"",
                    "nama_toko"=>"",
                    "desc_toko"=>"",
                    "main_img_toko"=>"",
                    "img_list_toko"=>""
                );

        if($this->val_form_update_toko()){
            $id_toko        = $this->input->post("id_toko", true);
            $tipe_owner     = $this->input->post("tipe_owner", true);
            $owner          = $this->input->post("owner", true);
            $nama_toko      = $this->input->post("nama_toko", true);
            $desc_toko      = $this->input->post("desc_toko", true);
            $main_img_toko  = $this->input->post("main_img_toko", true);
            $img_list_toko  = $this->input->post("img_list_toko", true);


            if(!$this->mm->get_data_each("toko", ["nama_toko"=>$nama_toko, "id_toko!="=>$id_toko])){

                $main_img_toko = str_replace(base_url(), "base_url/", $main_img_toko);
                $img_list_toko = str_replace(base_url(), "base_url/", $img_list_toko);
                $desc_toko = str_replace(base_url(), "base_url/", $desc_toko);
                
                $where = ["id_toko"=>$id_toko];

                $data = ["tipe_owner"    =>$tipe_owner,
                        "id_owner"         =>$owner,
                        "nama_toko"     =>$nama_toko,
                        "desc_toko"     =>$desc_toko,
                        "main_img_toko" =>$main_img_toko,
                        "img_list_toko" =>$img_list_toko
                    ];

                // $update = $this->mt->toko_insert($nama_toko, $deskripsi_toko, $id_user);
                $update = $this->mm->update_data("toko", $data, $where);

                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["tipe_owner"]     = strip_tags(form_error('tipe_owner'));
            $msg_detail["owner"]          = strip_tags(form_error('owner'));  
            $msg_detail["nama_toko"]      = strip_tags(form_error('nama_toko'));
            $msg_detail["desc_toko"]      = strip_tags(form_error('desc_toko'));  
            $msg_detail["main_img_toko"]  = strip_tags(form_error('main_img_toko'));
            $msg_detail["img_list_toko"]  = strip_tags(form_error('img_list_toko'));           
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_toko--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_toko--------------------------------
#===============================================================================

    public function delete_toko(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_toko"=>"",
                );

        if($_POST["id_toko"]){
        	$id_toko = $this->input->post("id_toko");
        	$where = array("id_toko"=>$id_toko);

            $set = array("is_delete"=>"1");
            $where = array("id_toko"=>$id_toko);

        	// $delete_toko = $this->mm->delete_data("toko", array("id_toko"=>$id_toko));
        	$delete_toko = $this->mm->update_data("toko", $set, $where);
            
            if($delete_toko){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_toko"]= strip_tags(form_error('id_toko'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("toko", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_toko--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------dasabled_toko------------------------------
#===============================================================================

    public function disabled_toko(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_toko"=>"",
                );

        if($_POST["id_toko"]){
        	$id_toko = $this->input->post("id_toko");
        	$where 	= array("id_toko"=>$id_toko);
        	$set 	= array("status_active"=>"0");
        	
        	$update_toko = $this->mm->update_data("toko", $set, $where);
        	if($update_toko){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_toko"]= strip_tags(form_error('id_toko'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("toko", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_toko------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------activate_toko------------------------------
#===============================================================================
    public function activate_toko(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_toko"=>"",
                );

        if($_POST["id_toko"]){
        	$id_toko = $this->input->post("id_toko");
        	$where 	= array("id_toko"=>$id_toko);
        	$set 	= array("status_active"=>"1");
        	
        	$update_toko = $this->mm->update_data("toko", $set, $where);
        	if($update_toko){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_toko"]= strip_tags(form_error('id_toko'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("toko", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_toko------------------------------
#===============================================================================


}
