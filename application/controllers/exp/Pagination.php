<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagination extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('main/main_product', 'mp');
        $this->load->model('homepage/main_homepage', 'mh');
        
        $this->load->library("magic_pattern");
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        $this->load->library('pagination');
        $this->load->library('Perpage');
        
        
        // $this->auth_v0->check_session_active_ad();
    }

    public function get_this_url(){
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
			$link = "https"; 
		else
			$link = "http"; 

		// Here append the common URL characters. 
		$link .= "://"; 

		// Append the host(domain name, ip) to the URL. 
		$link .= $_SERVER['HTTP_HOST']; 

		// Append the requested resource location to the URL 
		$link .= $_SERVER['REQUEST_URI']; 

		// Print the link 
		return $link; 
    }

	public function index(){
		$paginationlink = "getresult.php?page=";
		// $this->load->library('pagination');
		$per_page = 10;
		if(!empty($_GET["page"])) {
			$per_page = (int) $_GET["page"];
		}

		$start = 1;
		if(!empty($_GET["start"])) {
			$start = (int) $_GET["start"];
		}

		$output = $this->perpage->getAllPageLinks(78, $paginationlink, (int)$per_page);

		print_r($output);

		// $from = $this->uri->segment(4);		
		// $data['product'] = $this->mh->get_product_for_home_page([], "Daster", $config['per_page'], $from);

		// // print_r($this->pagination->create_links());
		// // print_r($count_data["sum"]);
		// // die();
		// $this->load->view('index', $data);
	}

	public function manual_pagination($per_page = 10, $limit = 1, $page = 1){
		$data["count_data"] 	= $this->mm->count_data("product", []);
		// print_r($data);
		// die();
		$data["count_page"] 	= ceil($data["count_data"]["sum"] / $per_page);
		// print_r($data);
		// die();
		$data["number_page"] 	= 10;

		$data["main_url"] 		= base_url()."exp/pagination/manual_pagination/";

		$str_nav = '<div class="pagging text-center">
						<nav>
							<ul class="pagination justify-content-center" style="font-size: large;">';

		if($data["count_page"] <= $data["number_page"] or $data["count_page"] > 0){
			$n_per_page = 0 ;
			$n_awal = 0;
			for ($i=$page; $i<$data["count_page"]+$page ; $i++) { 
				$str_nav .= '<li class="page-item">
								<span class="page-link">
									<a href="'.$data["main_url"].$per_page.'/'.$limit.'/'.$i.'" data-ci-pagination-page="2">'.$i.'</a>
								</span>
							</li>';
			}

			$str_nav .= '<li class="page-item">
								<span class="page-link">
									<a href="'.$data["main_url"].$per_page.'/'.$limit.'/'.$i.'" data-ci-pagination-page="2">'.$i.'</a>
								</span>
							</li>';
		}else{

		}

		$str_nav .= '		</ul>
						</nav>
					</div>';


		$data = $this->mp->get_product_all([], $per_page, $limit);

		print_r("<pre>");
		print_r($str_nav);
		// print_r($data);
	}

	
}
