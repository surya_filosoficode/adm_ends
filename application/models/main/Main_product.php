<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_product extends CI_Model{

    public function get_product_all($where, $per_page, $limit){
        $data = $this->db->get_where("product p", $where, $per_page, $limit);
        return $data->result();
    }

    public function get_product_each($where){
        $data = $this->db->get_where("product p", $where);
        return $data->result();
    }
}
?>