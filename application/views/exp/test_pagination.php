<!DOCTYPE html>
<html>
<head>
	<title>Membuat Pagination Pada CodeIgniter | MalasNgoding.com</title>
</head>

<?php
	$per_page = "10";
	if(isset($_GET["npage"])){
		$per_page = $_GET["npage"];				
	}

	$default_param = 0;
	if($this->uri->segment(4)){
		$default_param = $this->uri->segment(4);
	}
?>

<body>
	


	<div class="pagging text-center">
		<nav>
			<ul class="pagination justify-content-center" style="font-size: large;">
				<li class="page-item active">
					<span class="page-link">1<span class="sr-only">(current)</span></span>
				</li>
				<li class="page-item">
					<span class="page-link"><a href="http://localhost:8080/adm_ends/exp/pagination/10" data-ci-pagination-page="2">2</a></span>
				</li>
				
				<li class="page-item">
					<span class="page-link"><a href="http://localhost:8080/adm_ends/exp/pagination/10" data-ci-pagination-page="2" rel="next">Next</a></span>
				</li>

			</ul></nav></div>
<h1>Membuat Pagination Pada CodeIgniter | MalasNgoding.com</h1>
	<table border="1">
		<tr>
			<th>no</th>
			<th>nama_product</th>
			<th>price_product</th>	
		</tr>
		<?php 
		$no = $this->uri->segment('4') + 1;
		foreach($product as $u){ 
		?>
		<tr>
			<td><?php echo $no++; ?></td>
			<td><?php echo $u->nama_product ?></td>
			<td><?php echo $u->price_product ?></td>
		</tr>
	<?php } ?>
	</table>
	<br/>
	<select name="per_page" id="per_page">
		<option value="10">10</option>
		<option value="30">30</option>
		<option value="50">50</option>
		<option value="100">100</option>
	</select>
	&nbsp;&nbsp;&nbsp;
	<?php
	print_r("<pre>");
	echo $this->pagination->create_links();
	?>
</body>

<script type="text/javascript">
	$(document).ready(function(){
		$("#per_page").val('<?=$per_page?>');
	});

	$("#per_page").change(function() {
	  window.location.href = "<?=$main_url."/$default_param";?>/?npage="+$("#per_page").val();
	});
</script>
</html>