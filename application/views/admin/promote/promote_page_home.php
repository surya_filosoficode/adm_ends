<?php
    $ar_month = $this->time_master->set_indo_month();
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Data Jenis Product</h3>
    </div>
    
    <div class="col-md-6 text-right">
        <a class="btn btn-info" id="btn_add_article" href="<?php print_r(base_url());?>admin/data_post_article">Tambah Jenis Product</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Jenis Product</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No. </th>
                                        <th width="15">Id Jenis Product</th>
                                        <th width="*">Nama Jenis Product</th>
                                        <th width="20%">Action</th>
                                    </tr>
                                </thead>
                                <tbody id="main_table_content">
                                    <?php
                                        if(!empty($list_data)){
                                            $no = 1; 
                                            foreach ($list_data as $key => $value) {
                                                $id_jenis   = $value->id_jenis;
                                                $nama_jenis = $value->nama_jenis;
                                                $parent_id  = $value->parent_id;

                                                print_r("<tr>
                                                        <td>".$no."</td>
                                                        <td>".$id_jenis."</td>
                                                        <td>".$nama_jenis."</td>

                                                        <td>
                                                            <center>
                                                            
                                                            <button class=\"btn btn-primary\" id=\"del_article\" onclick=\"add_promote('".$value->id_jenis ."')\" style=\"width: 40px;\"><i class=\"fa fa-plus-square\"></i></button>
                                                            </center>
                                                        </td>
                                                    </tr>");

                                                $no++;
                                            }
                                        }
                                    ?>
                                    <!-- <button class=\"btn btn-info\" id=\"up_article\" onclick=\"detail_promote('".$value->id_jenis ."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\" ></i></button>&nbsp;&nbsp; -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Promote Jenis Product</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive m-t-40">
                            <table id="myTable1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%">No. </th>
                                        <th width="30%">(Id Jenis) Nama Jenis</th>
                                        <th width="10%">Letak Header</th>
                                        <th width="10%">Urutan Category</th>
                                        <th width="15%">Status Promote</th>
                                        <th width="15%">Status Aktif</th>
                                        <th width="15%">Action</th>
                                    </tr>

                                </thead>
                                <tbody id="main_table_content">
                                    <?php
                                        if(!empty($list_data_pr)){
                                            $no = 1; 
                                            foreach ($list_data_pr as $key => $value) {
                                                $id_pr      = $value->id_pr;
                                                $id_jenis   = $value->id_jenis;
                                                $nama_jenis = $value->nama_jenis;

                                                $align_header_pr = $value->align_header_pr;
                                                // $str_align_header = "<select class=\"form-control custom-select sts-all\" id=\"align_".$id_pr."\" name=\"align_".$id_pr."\">
                                                //             <option value=\"left\">left</option>
                                                //             <option value=\"right\">right</option>
                                                //         </select>";

                                                $r_num_pr   = $value->r_num_pr;
                                                // $str_num_pr = "<input type=\"number\" class=\"form-control num-all\" name=\"num_pr_".$id_pr."\" id=\"num_pr_".$id_pr."\" value=\"".$r_num_pr."\">";

                                                $tgl_add_pr = $value->tgl_add_pr;
                                                $add_by_pr  = $value->add_by_pr;

                                                $str_sts_pr = "none";
                                                $sts_pr     = $value->sts_pr;
                                                if($sts_pr == "1"){
                                                    $str_sts_pr = "promote";
                                                }

                                                // $str_in_sts_pr = "<select class=\"form-control custom-select sts-all\" id=\"sts_pr_".$id_pr."\" name=\"sts_pr_".$id_pr."\">
                                                //             <option value=\"0\">none</option>
                                                //             <option value=\"1\">promote</option>
                                                //         </select>";


                                                $str_active_pr = "non-active";
                                                $active_pr  = $value->active_pr;                                               
                                                if($active_pr == "1"){
                                                    $str_active_pr = "active";
                                                }

                                                // $str_in_sts_ac = "<select class=\"form-control custom-select sts-all\" id=\"sts_ac_".$id_pr."\" name=\"sts_ac_".$id_pr."\">
                                                //             <option value=\"0\">non-active</option>
                                                //             <option value=\"1\">active</option>
                                                //         </select>";

                                                print_r("<tr>
                                                        <td>".$no."</td>
                                                        <td>(".$id_jenis.") ".$nama_jenis."</td>
                                                        <td>".$align_header_pr."</td>
                                                        <td>".$r_num_pr."</td>
                                                        
                                                        <td>".$str_sts_pr."</td>
                                                        <td>".$str_active_pr."</td>

                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_article\" onclick=\"get_data('".$value->id_pr ."')\" style=\"width: 40px;\"><i class=\"fa fa-list-alt\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_article\" onclick=\"delete_promote('".$value->id_pr ."')\" style=\"width: 40px;\"><i class=\"fa fa-trash\"></i></button>
                                                            </center>
                                                        </td>
                                                    </tr>");

                                                $no++;
                                            }
                                        }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>


<div class="modal fade bs-example-modal-lg" id="modal_up" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Promote Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Keterangan</label>
                            <h5 id="keterangan"></h5>
                            <!-- <p id="msg_letak_header" style="color: red;"></p> -->
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Letak Header</label>
                            <!-- <input type="text" class="form-control" id="letak_header" name="letak_header" required=""> -->
                            <select class="form-control custom-select sts-all" id="letak_header" name="letak_header" required="">
                                <option value="left">left</option>
                                <option value="right">right</option>
                            </select>
                            <p id="msg_letak_header" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Urutan Category</label>
                            <input type="text" class="form-control" id="urutan_category" name="urutan_category" required="">
                            <p id="msg_urutan_category" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Status Promote</label>
                            <!-- <input type="text" class="form-control" id="status_promote" name="status_promote" required=""> -->
                            <select class="form-control custom-select sts-all" id="status_promote" name="status_promote" required="">
                                <option value="0">none</option>
                                <option value="1">promote</option>
                            </select>
                            <p id="msg_status_promote" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Status Aktif</label>
                            <!-- <input type="text" class="form-control" id="status_active" name="status_active" required=""> -->
                            <select class="form-control custom-select sts-all" id="status_active" name="status_active" required="">
                                <option value="0">non-active</option>
                                <option value="1">active</option>
                            </select>
                            <p id="msg_status_active" style="color: red;"></p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" id="b_up" class="btn btn-info waves-effect text-left">Ubah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>

<script type="text/javascript">
    var id_cache = "";

    function detail_promote(id_jenis){
        window.open("<?php print_r(base_url());?>admin/articlemain/index_update/"+id_jenis);
    }

    //=========================================================================//
    //-----------------------------------add_promote---------------------------//
    //=========================================================================//
        function add_promote(id_jenis){
            var data_main = new FormData();
            data_main.append('id_jenis', id_jenis);

            $.ajax({
                url: "<?php echo base_url()."admin/promotepagehome/add_promote";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_add_promote(res);
                }
            });
        }

        function response_add_promote(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/home_page_content");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------add_promote---------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------delete_promote------------------------//
    //=========================================================================//
        function delete_promote(id_pr){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_pr);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function method_delete(id_pr){
            var data_main = new FormData();
            data_main.append('id_pr', id_pr);

            $.ajax({
                url: "<?php echo base_url()."admin/promotepagehome/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/home_page_content");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------delete_promote------------------------//
    //=========================================================================// 


    //=========================================================================//
    //-----------------------------------up_promote----------------------------//
    //=========================================================================//
        function get_data(id_pr){
            var data_main = new FormData();
            data_main.append('id_pr', id_pr);

            $.ajax({
                url: "<?php echo base_url()."admin/promotepagehome/get";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_get_data(res, id_pr);
                }
            });
        }

        function response_get_data(res, id_pr) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(detail_msg);
            if (main_msg.status) {
                id_cache = id_pr;
                $("#modal_up").modal("show");

                var id_jenis    = detail_msg.list_data.id_jenis;
                var nama_jenis  = detail_msg.list_data.nama_jenis;

                console.log(nama_jenis);

                var letak_header    = detail_msg.list_data.align_header_pr;
                var urutan_category = detail_msg.list_data.r_num_pr;
                var status_promote  = detail_msg.list_data.sts_pr;
                var status_active   = detail_msg.list_data.active_pr;

                $("#keterangan").html("("+id_jenis+") "+nama_jenis);

                $("#letak_header").val(letak_header);
                $("#urutan_category").val(urutan_category);
                $("#status_promote").val(status_promote);
                $("#status_active").val(status_active);
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------up_promote----------------------------//
    //=========================================================================// 


    //=========================================================================//
    //-----------------------------------edit_promote--------------------------//
    //=========================================================================//
        $("#b_up").click(function(){
            up_promote();
        });

        function up_promote(id_jenis){
            var data_main = new FormData();
            data_main.append('id_pr', id_cache);

            data_main.append('letak_header'     , $("#letak_header").val());
            data_main.append('urutan_category'  , $("#urutan_category").val());
            data_main.append('status_promote'   , $("#status_promote").val());
            data_main.append('status_active'    , $("#status_active").val());


            $.ajax({
                url: "<?php echo base_url()."admin/promotepagehome/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_add_promote(res);
                }
            });
        }

        function response_add_promote(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/home_page_content");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------edit_promote--------------------------//
    //=========================================================================//

</script>