<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Create Article</h3>
    </div>

    <div class="col-md-6 text-right">
        <a class="btn btn-rounded btn-info" href="<?= base_url();?>admin/article_list"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;List Article</a>
        
        <!-- <button type="button" class="btn btn-rounded btn-danger" id="btn_del_article"><i class="fa fa-trash"></i>&nbsp;&nbsp;&nbsp;Hapus Article</button> -->

        <button type="button" class="btn btn-rounded btn-success" id="btn_add_article"><i class="fa fa-pencil"></i>&nbsp;&nbsp;&nbsp;Simpan Article</button>
        
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"><h5>Pilih Image</h5></div>
                <div class="card-body">
                    
                </div>
            </div>
        </div>
    </div> -->
    <div class="row" id="row_img_list">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Image List</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div id="slimtest1">
                            <div class="row el-element-overlay" id="out_img_list"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>           

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Pilih Image</h5>
                        </div>
                        <!-- <div class="col-md-6 text-right">
                            <a href="#" onclick="get_image()" style="font-size: 15px;"><i class="mdi mdi-folder-image"></i> &nbsp;&nbsp; Lihat Gambar</a>        
                        </div> -->
                    </div>
                </div>
                <div class="card-body">
                    <?php echo form_open_multipart('admin/postimagemain/save_article');?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Title</label>
                                        <input type="input" class="form-control" name="title" id="title">
                                        <p id="msg_title" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tipe Article</label>
                                        
                                        <select class="select2 form-control custom-select" name="tipe" id="tipe" style="width: 100%; height:36px;">
                                            <?php
                                                if(isset($list_tipe)){
                                                    if($list_tipe){
                                                        foreach ($list_tipe as $key => $value) {
                                                            print_r("<option value=\"".$value->nama_art_tipe."\">".$value->nama_art_tipe."</option>");
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <p id="msg_tipe" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Categori</label>
                                        
                                        <select class="select2 m-b-10 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose" name="cate" id="cate">
                                            <option value="default">Default</option>
                                            <?php
                                                if(isset($list_jenis)){
                                                    if($list_jenis){
                                                        foreach ($list_jenis as $key => $value) {
                                                            print_r("<option value=\"".$value->nama_jenis_article."\">".$value->nama_jenis_article."</option>");
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                        <p id="msg_cate" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Hastag</label><br>
                                        
                                        <select multiple data-role="tagsinput" name="tag" id="tag">
                                        </select>
                                        <p id="msg_tag" style="color: red;"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Image article Utama</label>
                                        <input type="input" class="form-control" name="img_utama" id="img_utama">
                                        <p id="msg_img_utama" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-3 text-left">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">&nbsp;</label><br>
                                        <button type="button" name="add_img_utama" id="add_img_utama" class="btn btn-info">add gambar utama</button>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card" style="margin-bottom: 10px; margin-top: 10px;">
                                            <div class="el-card-item" style="padding: 0px;">
                                                <div class="el-card-avatar el-overlay-1" style="margin: 0px;">
                                                    <img id="out_img_utama" name="out_img_utama" src="" width="290px" height="158px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Article</label>
                                <textarea class="summernote" name="article" id="article"></textarea>
                                <p id="msg_article" style="color: red;"></p>
                            </div>        
                        </div>
                        
                        <!-- <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12 text-right">
                            <input type="submit" id="save" class="btn btn-success btn-rounded" value="Save">
                        </div> -->
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->         
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

    <div class="modal fade bs-example-modal-lg" id="modal_img_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="d_title_img">Large modal</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label"><b>Categori/Hastag</b></label>
                                <h6 id="d_category_img"></h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <img src="" id="out_base_64" width="100%">  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

<?php
    if(isset($list_image)){
        if ($list_image) {
            $set_array = [];
            foreach ($list_image as $key => $value) {
                $id_img = $value->id_img;
                $title_img = $value->title_img;
                $path_img = $value->path_img;
                $file_img = $value->file_img;
                $category_img = json_decode($value->category_img);

                $set_array[$id_img] = ["title_img"=>$title_img,
                                        "path_img"=>$path_img,
                                        "file_img"=>$file_img,
                                        "category_img"=>$category_img,
                                    ];
            }

            $set_array = json_encode($set_array);
        }
    }
?>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>
    <script>
    var all_var = "";
    var list_image = JSON.parse('<?php print_r($set_array);?>');



    jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

        $("#row_img_list").hide(100);

    });

    $('#slimtest1').slimScroll({
        height: '400px'
    });

    $(document).ready(function(){
        render_img(list_image);
    });

    //=========================================================================//
    //-----------------------------------image_action--------------------------//
    //=========================================================================//
        function render_img(data_json){
            // console.log(data_json);
            var str_img = "";
            for (let item in data_json) {
                // console.log(data_json[item]);
                var title_img_show = data_json[item].category_img.join(", ");

                if(title_img_show >= 18){
                    var title_img_show = title_img_show.substr(0, 18) + " ...";
                }

                str_img += "<div class=\"col-lg-3 col-md-6\">"+
                                "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                    "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                        "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                            "<img src=\"<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"\">"+
                                            "<div class=\"el-overlay\">"+
                                                "<ul class=\"el-info\">"+
                                                    "<li><a class=\"btn default btn-outline\" onclick=\"open_image('"+item+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                    "<li><a class=\"btn default btn-outline\" href=\"javascript:void(0);\" onclick=\"copy_clip('<?php print_r(base_url());?>"+data_json[item]["path_img"]+data_json[item].file_img+"')\"><i class=\"icon-link\"></i></a></li>"+
                                                "</ul>"+
                                            "</div>"+
                                            "<div style=\"padding: 2px;\">"+
                                                "<label>"+title_img_show+"</label>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>";
            }

            $("#out_img_list").html(str_img);
            $("#row_img_list").show(200);
        }

        function copy_clip(text){
            copyTextToClipboard(text);
        }

        function sip(){
            console.log(all_var);
        }

        function open_image(id_item){
            var main_img     = "<?php print_r(base_url()); ?>"+list_image[id_item].path_img+list_image[id_item].file_img;
            var title_img    = list_image[id_item].title_img;
            var category_img = list_image[id_item].category_img.join(", ");

            $("#d_title_img").html(title_img);
            $("#d_category_img").html(category_img);
            
            $("#out_base_64").attr("src", main_img);

            $("#modal_img_detail").modal("show");
        }
    //=========================================================================//
    //-----------------------------------image_action--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------add_main_image------------------------//
    //=========================================================================//
        $("#add_img_utama").click(function(){
            var img_utama = $("#img_utama").val();

            $("#out_img_utama").attr("src", img_utama);
        });
    //=========================================================================//
    //-----------------------------------add_main_image------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------add_post------------------------------//
    //=========================================================================//
        function clear_form_add(){
            $("#title").val("");
            $("#cate").val(null).trigger("change");
            $("#tipe").val(null).trigger("change");
            $('#tag').tagsinput('removeAll');

            $("#add_img_utama").val("");
            $("#img_utama").attr('src', "")

            $("#article").html("");
        }

        $("#btn_add_article").click(function(){
            add_article();
        });


        function add_article(){
            var data_main = new FormData();
            data_main.append('title_article'    , $("#title").val());
            data_main.append('tipe_article'     , $("#tipe").val());
            data_main.append('category_article' , JSON.stringify($("#cate").val()));
            data_main.append('tag_article'      , JSON.stringify($("#tag").tagsinput('items')));
            data_main.append('main_img_article' , $("#out_img_utama").attr('src'));
            data_main.append('content_article'  , $("#article").val());

            // console.log();
            
            $.ajax({
                url: "<?php echo base_url()."admin/articlemain/save";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_insert(res);
                }
            });
        }

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                clear_form_add();
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/article_list");?>");
            } else {
                $("#msg_tipe").html(detail_msg.tipe_article);
                $("#msg_tipe_owner").html(detail_msg.tipe_owner);
                $("#msg_owner").html(detail_msg.owner);
                $("#msg_nama_toko").html(detail_msg.nama_toko);
                $("#msg_article").html(detail_msg.desc_toko);
                $("#msg_img_utama").html(detail_msg.main_img_toko);
                $("#msg_img_list_toko").html(detail_msg.img_list_toko);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------add_post------------------------------//
    //=========================================================================//

    

    </script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>
