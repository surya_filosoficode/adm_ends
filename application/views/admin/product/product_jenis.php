<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Data Jenis Product</h3>
    </div>

    <div class="col-md-6 text-right">
        <!-- <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#modal_add_jenis"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Data Jenis Product</button> -->
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<?php
    $str_op = "";
    if(isset($list_data)){
        if($list_data){
            foreach ($list_data as $key => $value) {
                $str_op .= "<option value=\"".$value->id_jenis."\">".$value->nama_jenis."</option>";
            }
        }
    }
?>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Input Data Jenis Product</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <form action="javascript:void(0)">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Jenis Product</label>
                                    <input type="text" class="form-control" id="nama_jenis" name="nama_jenis" required="">
                                    <p id="msg_nama_jenis" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Parent Jenis</label>
                                    <select class="select2 form-control custom-select" id="parent_jenis" name="parent_jenis" style="width: 100%; height:36px;">
                                        <option value="0">None</option>
                                        <?= $str_op; ?>
                                    </select>
                                    <p id="msg_parent_jenis" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="submit" id="save_jenis" class="btn btn-info waves-effect text-left">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Jenis Product</h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive m-t-40">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="15%">No. </th>
                                        <th width="*">Nama Jenis</th>
                                        <th width="20%">Parent Jenis</th>
                                        <th width="20%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="main_table_content">
                                    <?php
                                        if(!empty($list_data)){
                                            foreach ($list_data as $r_jenis => $v_jenis) {
                                                $btn_act = "";
                                                echo "<tr>
                                                        <td>".($r_jenis+1)."</td>
                                                        <td>".$v_jenis->nama_jenis."</td>
                                                        <td>".$v_jenis->parent_id."</td>
                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_jenis\" onclick=\"update_jenis('".$v_jenis->id_jenis."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_jenis\" onclick=\"delete_jenis('".$v_jenis->id_jenis."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                    </tr>";
                                            }
                                        }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>




<div class="modal fade bs-example-modal-lg" id="modal_up_jenis" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="javascript:void(0)">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update brand</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Jenis Product</label>
                                <input type="text" class="form-control" id="_nama_jenis" name="nama_jenis" required="">
                                <p id="msg_nama_jenis" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Parent Jenis</label>
                                <select class="select2 form-control custom-select" id="_parent_jenis" name="parent_jenis" style="width: 100%; height:36px;">
                                    <option value="0">None</option>
                                    <?= $str_op; ?>
                                </select>
                                <p id="_msg_parent_jenis" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    <button type="submit" id="b_up_jenis" class="btn btn-info waves-effect text-left">Ubah</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

   

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#save_jenis").click(function() {
            var data_main = new FormData();
            data_main.append('nama_jenis', $("#nama_jenis").val());
            data_main.append('parent_jenis', $("#parent_jenis").val());
            
            $.ajax({
                url: "<?php echo base_url()."admin/Productjenis/save";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                // $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/jenis_product");?>");
            } else {
                $("#msg_nama_jenis").html(detail_msg.nama_jenis);
                $("#msg_parent_jenis").html(detail_msg.parent_jenis);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_nama_jenis").html("");
            $("#msg_parent_jenis").html("");
        }

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_msg_nama_jenis").html("");
            $("#_msg_parent_jenis").html("");
        }

        function update_jenis(id_jenis) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_jenis', id_jenis);

            $.ajax({
                url: "<?php echo base_url()."admin/Productjenis/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_jenis);
                    $("#modal_up_jenis").modal('show');
                }
            });
        }

        function set_val_update(res, id_jenis) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_jenis;

                // console.log(list_data.parent_jenis);

                $("#_nama_jenis").val(list_data.nama_jenis);
                $("#_parent_jenis").val(list_data.parent_id);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#b_up_jenis").click(function() {
            var data_main = new FormData();
            data_main.append('id_jenis', id_cache);

            data_main.append('nama_jenis', $("#_nama_jenis").val());
            data_main.append('parent_jenis', $("#_parent_jenis").val());

            console.log($("#_parent_jenis").val());
            $.ajax({
                url: "<?php echo base_url()."admin/Productjenis/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_admin').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/jenis_product");?>");
            } else {
                $("#_msg_nama_jenis").html(detail_msg.nama_jenis);
                 $("#_msg_parent_jenis").html(detail_msg.parent_jenis);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_jenis){
            var data_main = new FormData();
            data_main.append('id_jenis', id_jenis);

            $.ajax({
                url: "<?php echo base_url()."admin/Productjenis/delete";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_jenis(id_jenis) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_jenis);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/jenis_product");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//
</script>

