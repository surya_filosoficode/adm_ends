<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Pilih Toko</h3>
    </div>
    
    <div class="col-md-6 text-right">
        <a class="btn btn-info" id="btn_add_toko" href="<?php print_r(base_url());?>admin/toko_main">Tambah Toko</a>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <!-- Card -->
                <div class="card">
                    <div class="card-body">
                        <div class="row el-element-overlay">
                            <?php
                                if(isset($list_toko)){
                                    if($list_toko){
                                        foreach ($list_toko as $key => $value) {
                                            // print_r($value);
                                            $id_toko        = $value->id_toko;
                                            $main_img_toko  = $value->main_img_toko;
                                            $nama_toko      = $value->nama_toko;
                                            $tipe_owner     = $value->tipe_owner;
                                            $id_owner       = $value->id_owner;

                                            $main_img_toko = str_replace("base_url/", base_url(), $main_img_toko);
                                            $url_next_product = base_url()."admin/main_product/".$id_toko;
                            ?>
                                <div class="col-lg-3 col-md-6">
                                    <div class="card">
                                        <div class="el-card-item" style="padding: 0px;">
                                            <div class="el-card-avatar el-overlay-1"> <img src="<?=$main_img_toko?>" alt="user" />
                                                <div class="el-overlay scrl-up">
                                                    <ul class="el-info">
                                                        <li>
                                                            <a class="btn default btn-outline" onclick="detail_toko('<?=$id_toko?>')" href="javascript:void(0);">
                                                                <i class="icon-list"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="el-card-content">
                                                <b><h3 class="box-title"><a href="<?=$url_next_product?>"><?=$nama_toko?></a></h3></b>
                                                <h6><a href="<?=$url_next_product?>">by. <?=$id_owner?></a></h6>
                                                <br/> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                        }
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- Card -->
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url());?>assets/template/assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>

<script type="text/javascript">
    function detail_toko(id_toko){
        window.location.href = "<?php print_r(base_url());?>admin/product_list_af_toko/"+id_toko;
    }

    //=========================================================================//
    //-----------------------------------delete_toko---------------------------//
    //=========================================================================//
        function delete_toko(id_toko){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_toko);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function method_delete(id_toko){
            var data_main = new FormData();
            data_main.append('id_toko', id_toko);

            $.ajax({
                url: "<?php echo base_url()."admin/tokomain/delete_toko";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/toko_list");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------delete_toko---------------------------//
    //=========================================================================// 
</script>