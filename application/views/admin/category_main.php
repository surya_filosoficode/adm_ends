<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Data Category</h3>
    </div>

    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#modal_add_category"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Data Category</button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <!-- Card -->
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="15%">No. </th>
                                    <th width="*">Category</th>
                                    <th width="20%">Parent Id</th>
                                    <th width="20%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="main_table_content">
                                <?php
                                    if(!empty($list_data)){
                                        foreach ($list_data as $r_category => $v_category) {
                                            $btn_act = "";
                                            echo "<tr>
                                                    <td>".($r_category+1)."</td>
                                                    <td>".$v_category->ct."</td>
                                                    <td>".$v_category->id_img_ct_base."</td>
                                                    <td>
                                                        <center>
                                                        <button class=\"btn btn-info\" id=\"up_category\" onclick=\"update_category('".$v_category->id_img_ct."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                        <button class=\"btn btn-danger\" id=\"del_category\" onclick=\"delete_category('".$v_category->id_img_ct."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                        </center>
                                                    </td>
                                                </tr>";
                                        }
                                    }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>


<div class="modal fade bs-example-modal-lg" id="modal_add_category" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="javascript:void(0)">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Category</label>
                                <input type="text" class="form-control" id="ket_category" name="ket_category" required="">
                                <p id="msg_ket_category" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_category" class="btn btn-info waves-effect text-left">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade bs-example-modal-lg" id="modal_up_category" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="javascript:void(0)">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Category</label>
                                <input type="text" class="form-control" id="_ket_category" name="ket_category" required="">
                                <p id="_msg_ket_category" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    <button type="submit" id="b_up_category" class="btn btn-info waves-effect text-left">Ubah</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#save_category").click(function() {
            var data_main = new FormData();
            data_main.append('ket_category', $("#ket_category").val().toLowerCase());
            

            $.ajax({
                url: "<?php echo base_url()."admin/imagemain/save_image_ct";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/category");?>");
            } else {
                $("#msg_ket_category").html(detail_msg.ket_category);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_ket_category").html("");
        }

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_msg_ket_category").html("");
        }

        function update_category(id_category) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_img_ct', id_category);

            $.ajax({
                url: "<?php echo base_url()."admin/imagemain/get_data_ct";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_category);
                    $("#modal_up_category").modal('show');
                }
            });
        }

        function set_val_update(res, id_category) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_category;

                $("#_ket_category").val(list_data.ct);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#b_up_category").click(function() {
            var data_main = new FormData();
            data_main.append('id_img_ct', id_cache);

            data_main.append('ket_category', $("#_ket_category").val().toLowerCase());

            $.ajax({
                url: "<?php echo base_url()."admin/imagemain/update_image_ct";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_admin').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/category");?>");
            } else {
                $("#_msg_ket_category").html(detail_msg.ket_category);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_category){
            var data_main = new FormData();
            data_main.append('id_img_ct', id_category);

            $.ajax({
                url: "<?php echo base_url()."admin/imagemain/delete_image_ct";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_category(id_category) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_category);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."admin/category");?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//
</script>

