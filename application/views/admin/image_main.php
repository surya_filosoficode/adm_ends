            <div class="row page-titles">
                <div class="col-md-6 align-self-center">
                    <h3 class="text-themecolor">Master Image</h3>
                </div>
                <div class="col-md-6 text-right">
                    <a class="btn btn-info" id="form_add_image" href="<?php print_r(base_url());?>admin/data_image_list">List Gambar</a>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <!-- .Your image -->
                                    <div class="col-md-12 p-20">
                                        <div class="img-container">
                                            <img id="image" src="<?php print_r(base_url());?>assets/template/assets/images/big/img2.jpg" class="img-responsive" alt="Picture">
                                        </div>
                                    </div>
                                    <!-- /.Your image -->
                                    <!-- .Croping image -->
                                    <div class="col-md-12 p-20">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="message-text" class="control-label">Title Gambar</label>
                                                            <input type="text" class="form-control" id="title_img" name="title_img" required="">
                                                            <p id="msg_title_img" style="color: red;"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="docs-preview clearfix">
                                                            <div class="img-preview" style="width: 358px; height: 231px;"></div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="message-text" class="control-label">Category Gambar</label>
                                                            
                                                            <div class="tags-default">
                                                                <!-- <input type="text" list="dl_ct_img" id="category_img" name="category_img" required="" class="form-control" placeholder="add tags" />

                                                                <datalist id="dl_ct_img">
                                                                    <option>ok</option>
                                                                </datalist> -->
                                                                <select class="select2 m-b-10 select2-multiple" id="category_img" name="category_img" style="width: 100%" multiple="multiple" data-placeholder="Choose">
                                                                    <?php
                                                                        if(!empty($list_data)){
                                                                            foreach ($list_data as $key => $value) {
                                                                                print_r("<option value=\"".$value->ct."\">".$value->ct."</option>");
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>

                                                                <p>
                                                                    Categori Tidak di temukan..? <a href="#" onclick="add_modal_category()"><u>CLICK DISINI untuk menambah category</u></a>, dan 
                                                                    <a href="#" onclick="go_list_category()"><u>list category</u></a>
                                                                </p>
                                                            </div>
                                                            <p id="msg_category_img" style="color: red;"></p>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="col-md-12" id="out_list_category">
                                                        <button id="cl_main_form">clear</button>    
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12 docs-buttons">
                                                        <div class="btn-group btn-group-crop">
                                                            <!-- <div class="col-md-3 "> -->
                                                                <!-- .btn groups -->
                                                                <div class="btn-group btn-group-justified docs-toggles" data-toggle="buttons">
                                                                    <label class="btn btn-secondary btn-outline">
                                                                        <input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">
                                                                        <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="aspectRatio: 16 / 9"> Product </span> </label>
                                                                    <label class="btn btn-secondary btn-outline">
                                                                        <input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="2.508223684210">
                                                                        <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="aspectRatio: 4 / 3"> Header </span> </label>
                                                                    <label class="btn btn-secondary btn-outline">
                                                                        <input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">
                                                                        <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="aspectRatio: 1 / 1"> Photo Profil </span> </label>
                                                                    <!-- <label class="btn btn-secondary btn-outline">
                                                                        <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">
                                                                        <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="aspectRatio: NaN" aria-describedby="tooltip648574"> Free </span> </label> -->
                                                                </div>
                                                                <!-- /.btn groups -->
                                                            <!-- </div> -->
                                                        </div>

                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-info" data-method="setDragMode" data-option="move" title="Move"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setDragMode&quot;, &quot;move&quot;)"> <span class="fa fa-arrows"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-info" data-method="setDragMode" data-option="crop" title="Crop"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;setDragMode&quot;, &quot;crop&quot;)"> <span class="fa fa-crop"></span> </span>
                                                            </button>
                                                        </div>
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-success" data-method="zoom" data-option="0.1" title="Zoom In"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, 0.1)"> <span class="fa fa-search-plus"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-success" data-method="zoom" data-option="-0.1" title="Zoom Out"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, -0.1)"> <span class="fa fa-search-minus"></span> </span>
                                                            </button>
                                                        </div>
                                                        <!-- <div class="btn-group">
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="-10" data-second-option="0" title="Move Left"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, -10, 0)"> <span class="fa fa-arrow-left"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="10" data-second-option="0" title="Move Right"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 10, 0)"> <span class="fa fa-arrow-right"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="0" data-second-option="-10" title="Move Up"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 0, -10)"> <span class="fa fa-arrow-up"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="move" data-option="0" data-second-option="10" title="Move Down"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;move&quot;, 0, 10)"> <span class="fa fa-arrow-down"></span> </span>
                                                            </button>
                                                        </div>
 -->
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="scaleX" data-option="-1" title="Flip Horizontal"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;scaleX&quot;, -1)"> <span class="fa fa-arrows-h"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="scaleY" data-option="-1" title="Flip Vertical"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;scaleY&quot;, -1)"> <span class="fa fa-arrows-v"></span> </span>
                                                            </button>
                                                        </div>

                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="rotate" data-option="-45" title="Rotate Left"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, -45)"> <span class="fa fa-rotate-left"></span> </span>
                                                            </button>
                                                            <button type="button" class="btn btn-secondary btn-outline" data-method="rotate" data-option="45" title="Rotate Right"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;rotate&quot;, 45)"> <span class="fa fa-rotate-right"></span> </span>
                                                            </button>
                                                        </div>

                                                        <div class="btn-group btn-group-crop">
                                                            <button style="width: 45px;" type="button" class="btn btn-secondary btn-warning" data-method="reset" title="Reset"> <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;reset&quot;)"> <span class="fa fa-refresh"></span> </span>
                                                            </button>
                                                            <label style="width: 45px;" class="btn btn-secondary btn-outline btn-primary" for="inputImage" title="Upload image file">
                                                                <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
                                                                <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs"> <span style="color: #ffffff;" class="fa fa-upload"></span> </span>
                                                            </label> 
                                                        </div>


                                                        <div class="btn-group btn-group-crop" style="margin-left: 5px;">
                                                            <!-- <div class="btn-group btn-group-crop"> -->
                                                                <button type="button" class="btn btn-success" id="btn_crop_x" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 1800, &quot;height&quot;: 1229 }" style="width: 45px;"> 
                                                                    <span class="fa fa-plus"></span>
                                                                </button>
                                                                <button type="button" class="btn btn-info" id="simpan"> 
                                                                    <span class="docs-tooltip">Simpan</span>
                                                                </button>
                                                            <!-- </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <hr>
                                                        
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="row el-element-overlay" id="list_img_send">
                                                                      
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.Croping of image -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- <button onclick="del_image('1')"></button> -->

            <div class="modal fade bs-example-modal-lg" id="modal_img_detail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="d_title_img">Large modal</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label"><b>Categori/Hastag</b></label>
                                        <h6 id="d_category_img"></h6>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <img src="" id="out_base_64" width="100%">  
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>


            <div class="modal fade bs-example-modal-lg" id="modal_add_category" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Nama Category</label>
                                        <input type="text" class="form-control" id="ket_category" name="ket_category" required="">
                                        <p id="msg_ket_category" style="color: red;"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                            <button type="button" id="save_category" class="btn btn-info waves-effect text-left">Simpan</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            
<!-- <a></a> -->
<script src="<?php print_r(base_url());?>assets/js/custom/main_custom.js"></script>
<script type="text/javascript">
    var obj_img = [];
    var img_for = "product";
    var default_size = "{ &quot;width&quot;: 1800, &quot;height&quot;: 1229 }";

    $(document).ready(function(){
        change_ratio();
    });

    $("input[name='aspectRatio']").change(function(){
        change_ratio();
    });

    function change_ratio(){
        var val_ratio = $("input[name='aspectRatio']:checked").attr("id");
        // console.log(val_ratio);
        default_size = "{ &quot;width&quot;: 1800, &quot;height&quot;: 1229 }";
        if(val_ratio == "aspectRatio0"){
            default_size = "{ &quot;width&quot;: 1800, &quot;height&quot;: 1229 }";
            img_for = "product";
             // id="btn_crop_x" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 1800, &quot;height&quot;: 1229 }"

        }else if(val_ratio == "aspectRatio1"){
            default_size = "{ &quot;width&quot;: 1525, &quot;height&quot;: 608 }";
            img_for = "header";
        }else if(val_ratio == "aspectRatio2"){
            default_size = "{ &quot;width&quot;: 500, &quot;height&quot;: 500 }";
            img_for = "profil";
        }
        $("#btn_crop_x").attr("data-option", default_size);
    }

    $(function () {
        'use strict';
        var console = window.console || { log: function () {} };
        var $image = $('#image');
        var $download = $('#download');
        var $dataX = $('#dataX');
        var $dataY = $('#dataY');
        var $dataHeight = $('#dataHeight');
        var $dataWidth = $('#dataWidth');
        var $dataRotate = $('#dataRotate');
        var $dataScaleX = $('#dataScaleX');
        var $dataScaleY = $('#dataScaleY');
        var options = {
            aspectRatio: 16 / 9,
            preview: '.img-preview',
            crop: function (e) {
              $dataX.val(Math.round(e.x));
              $dataY.val(Math.round(e.y));
              $dataHeight.val(Math.round(e.height));
              $dataWidth.val(Math.round(e.width));
              $dataRotate.val(e.rotate);
              $dataScaleX.val(e.scaleX);
              $dataScaleY.val(e.scaleY);
            }
          };


        // Tooltip
        $('[data-toggle="tooltip"]').tooltip();


        // Cropper
        $image.on({
            'build.cropper': function (e) {
            console.log(e.type);
        },
            'built.cropper': function (e) {
            console.log(e.type);
        },
            'cropstart.cropper': function (e) {
            console.log(e.type, e.action);
        },
            'cropmove.cropper': function (e) {
            console.log(e.type, e.action);
        },
            'cropend.cropper': function (e) {
            console.log(e.type, e.action);
        },
            'crop.cropper': function (e) {
            console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
        },
            'zoom.cropper': function (e) {
            console.log(e.type, e.ratio);
        }
        }).cropper(options);


        // Buttons
        if (!$.isFunction(document.createElement('canvas').getContext)) {
            $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
        }

        if (typeof document.createElement('cropper').style.transition === 'undefined') {
            $('button[data-method="rotate"]').prop('disabled', true);
            $('button[data-method="scale"]').prop('disabled', true);
        }


        // Download
        // if (typeof $download[0].download === 'undefined') {
        //     $download.addClass('disabled');
        // }


        // Options
        $('.docs-toggles').on('change', 'input', function () {
            var $this = $(this);
            var name = $this.attr('name');
            var type = $this.prop('type');
            var cropBoxData;
            var canvasData;

            if (!$image.data('cropper')) {
                return;
            }

            if (type === 'checkbox') {
                options[name] = $this.prop('checked');
                cropBoxData = $image.cropper('getCropBoxData');
                canvasData = $image.cropper('getCanvasData');

                options.built = function () {
                $image.cropper('setCropBoxData', cropBoxData);
                $image.cropper('setCanvasData', canvasData);
                };
            } else if (type === 'radio') {
            options[name] = $this.val();
            }

            $image.cropper('destroy').cropper(options);
        });


        // Methods
        $('.docs-buttons').on('click', '[data-method]', function () {
            var $this = $(this);
            var data = $this.data();
            var $target;
            var result;

            if ($this.prop('disabled') || $this.hasClass('disabled')) {
                return;
            }

            if ($image.data('cropper') && data.method) {
                data = $.extend({}, data); // Clone a new one

                if (typeof data.target !== 'undefined') {
                    $target = $(data.target);
                    if (typeof data.option === 'undefined') {
                        try {
                            data.option = JSON.parse($target.val());
                        } catch (e) {
                            console.log(e.message);
                        }
                    }
                }

                if (data.method === 'rotate') {
                    $image.cropper('clear');
                }

                result = $image.cropper(data.method, data.option, data.secondOption);
                console.log($image.cropper(data.method, data.option, data.secondOption));

                if (data.method === 'rotate') {
                    $image.cropper('crop');
                }

                switch (data.method) {
                    case 'scaleX':
                    case 'scaleY':
                      $(this).data('option', -data.option);
                      break;

                    case 'getCroppedCanvas':
                        if (result) {
                            
                            // console.log(result.toDataURL('image/jpeg'));
                            // Bootstrap's Modal
                            // $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);
                            if(result.toDataURL('image/jpeg')){
                                var category_img = $("#category_img").val();
                                // console.log(category_img);
                                var title_img = $("#title_img").val();
                                if(title_img){
                                    var tmp_arr = {"title_img":title_img,
                                                    "img_for":img_for,
                                                    "main_img":result.toDataURL('image/jpeg'),
                                                    "category_img":category_img
                                                };

                                    obj_img.push(tmp_arr);
                                    cl_main_form();

                                    create_alert("Success", "Foto berhasil disimpan dalam list", "success");

                                    console.log(obj_img);
                                    render_img_list();
                                }else{
                                    $("#msg_title_img").html("Title Gambar Harus di Isi");
                                }
                            }else{
                                create_alert("Failure", "Foto gagal disimpan dalam list", "error")
                            }
                            
                            // if (!$download.hasClass('disabled')) {
                            //   $download.attr('href', result.toDataURL('image/jpeg'));
                            // }
                        }
                    break;
                }

                if ($.isPlainObject(result) && $target) {
                    try {
                        $target.val(JSON.stringify(result));
                    } catch (e) {
                        console.log(e.message);
                    }
                }

            }
        });

        // Keyboard
        $(document.body).on('keydown', function (e) {

            if (!$image.data('cropper') || this.scrollTop > 300) {
                return;
            }

            switch (e.which) {
                case 37:
                    e.preventDefault();
                    $image.cropper('move', -1, 0);
                    break;

                case 38:
                    e.preventDefault();
                    $image.cropper('move', 0, -1);
                    break;

                case 39:
                    e.preventDefault();
                    $image.cropper('move', 1, 0);
                    break;

                case 40:
                    e.preventDefault();
                    $image.cropper('move', 0, 1);
                    break;
            }
        });


        // Import image
        var $inputImage = $('#inputImage');
        var URL = window.URL || window.webkitURL;
        var blobURL;

        if (URL) {
            $inputImage.change(function () {
                var files = this.files;
                var file;

                if (!$image.data('cropper')) {
                    return;
                }

                if (files && files.length) {
                file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        blobURL = URL.createObjectURL(file);
                        $image.one('built.cropper', function () {

                        // Revoke when load complete
                        URL.revokeObjectURL(blobURL);
                        }).cropper('reset').cropper('replace', blobURL);
                        $inputImage.val('');
                    } else {
                        window.alert('Please choose an image file.');
                    }
                }
            });
        } else {
            $inputImage.prop('disabled', true).parent().addClass('disabled');
        }
    });

    function render_img_list(){
        var str_img = "";
        for(let item in obj_img){
            // str_img += "<div style=\"padding: 10px\"><img src=\""+obj_img[item]+"\" style=\"display: block; width: 173.333px; height: 115.491px; min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; transform: translateX(-17.3333px) translateY(-18.7453px);\"></div>";

            var title_img_show = obj_img[item].title_img;

            if(obj_img[item].title_img.length >= 18){
                var title_img_show = obj_img[item].title_img.substr(0, 18) + " ...";
            }
            // console.log()

            

            str_img += "<div class=\"col-lg-3 col-md-6\">"+
                            "<div class=\"card\" style=\"margin-bottom: 10px; margin-top: 10px;\">"+
                                "<div class=\"el-card-item\" style=\"padding: 0px;\">"+
                                    "<div class=\"el-card-avatar el-overlay-1\" style=\"margin: 0px;\">"+
                                        "<img src=\""+obj_img[item].main_img+"\">"+
                                        "<div class=\"el-overlay\">"+
                                            "<ul class=\"el-info\">"+
                                                "<li><a class=\"btn default btn-outline\" onclick=\"open_image('"+item+"')\"><i class=\"icon-magnifier\"></i></a></li>"+
                                                "<li><a class=\"btn default btn-outline\" onclick=\"del_image('"+item+"')\"><i class=\"icon-close\"></i></a></li>"+
                                            "</ul>"+
                                        "</div>"+
                                    "</div>"+
                                    "<div style=\"padding: 2px;\">"+
                                        "<label>"+title_img_show+"</label>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";
        }

        $("#list_img_send").html(str_img);
    }

    function del_image(id_item){
        delete obj_img[id_item];
        create_alert("Success", "Foto berhasil dihapus dari list", "error");
        render_img_list();
    }

    function open_image(id_item){
        var main_img     = obj_img[id_item].main_img;
        var title_img    = obj_img[id_item].title_img;
        var category_img = obj_img[id_item].category_img.join(", ");

        $("#d_title_img").html(title_img);
        $("#d_category_img").html(category_img);
        
        $("#out_base_64").attr("src", main_img);

        $("#modal_img_detail").modal("show");
    }

    $("#simpan").click(function(){
        var data_main = new FormData();
        data_main.append('data_img', JSON.stringify(obj_img));

        $.ajax({
            url: "<?php echo base_url()."admin/imagemain/save_image";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                response_act(res);
                console.log(res);
            }
        });
    });

    function response_act(res){
        console.log(res);
        var data_json = JSON.parse(res);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        // console.log(data_json);
        if (main_msg.status) {
            create_sweet_alert("Pesan Success!!", "Gambar berhasil disimpan !!", "success", "<?php print_r(base_url()); ?>admin/data_image_list");
        } else {
            create_sweet_alert("Pesan Gagal!!", "Gambar ini Gagal disimpan", "warning", "");
        }
    }

    function cl_main_form(){
        $("#title_img").val("");
        $("#category_img").val(null).trigger("change");
        $("#out_base_64").attr("src", "");
    }

    function add_modal_category(){
        $("#modal_add_category").modal("show");
    }

    function go_list_category() {
        window.location.href = "<?php print_r(base_url()."admin/category");?>";    
    }


    //=========================================================================//
    //-----------------------------------category------------------------------//
    //=========================================================================//
        $("#save_category").click(function() {
            var data_main = new FormData();
            data_main.append('ket_category', $("#ket_category").val().toLowerCase());
            

            $.ajax({
                url: "<?php echo base_url()."admin/imagemain/save_image_ct";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    // console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json   = JSON.parse(res);
            var main_msg    = data_json.msg_main;
            var detail_msg  = data_json.msg_detail;

            var list_data   = data_json.list_data;
            if (main_msg.status) {
                clear_form_insert();

                create_alert("Proses Berhasil", main_msg.msg, "success");

                refresh_category(list_data);


            } else {
                $("#msg_ket_category").html(detail_msg.ct);

                create_alert("Proses Gagal", main_msg.msg, "error");
            }
        }

        function clear_form_insert(){
            $("#msg_ket_category").html("");

            $("#modal_add_category").modal("toggle");
        }

        function refresh_category(list_data){
            var str_option = "";
            for(let i in list_data){
                // console.log(i);
                str_option += "<option value=\""+list_data[i].ct+"\">"+list_data[i].ct+"</option>";
            }

            $("#category_img").html(str_option);

            // console.log(list_data);
        }
    //=========================================================================//
    //-----------------------------------category------------------------------//
    //=========================================================================//


</script>
